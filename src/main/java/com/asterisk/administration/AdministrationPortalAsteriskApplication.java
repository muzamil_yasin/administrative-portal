package com.asterisk.administration;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AdministrationPortalAsteriskApplication {

	public static void main(String[] args) {
		SpringApplication.run(AdministrationPortalAsteriskApplication.class, args);
	}

}
